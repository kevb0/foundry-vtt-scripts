const form = `
  <div style="display: inline-block; width: 150px">Weapon Type:</div>
  <select id="weaponType" />
    <option value="Rifle">Rifle</option>
    <option value="Carbine/Pistol/SMG">Carbine/Pistol/SMG</option>
    <option value="Other">Other</option>
    <option value="Rifle (Optics +1)">Rifle (Optics +1)</option>
    <option value="Rifle (Optics +2)">Rifle (Optics +2)</option>
  </select>
  <div style="display: inline-block; width: 150px">Aim Type:</div>
  <select id="aimType" />
    <option value="-2">Unaimed Shot (Slow) (-1/-2)</option>
    <option value="0">Aimed - No Optics (Slow) (+0)</option>
    <option value="1">Aimed - Optics (Slow) (+1/+2)</option>
  </select>
  <br />
  <div style="display: inline-block; width: 150px">Distance:</div>
  <select id="distance" />
    <option value="0">Short (+0)</option>
    <option value="-1">Medium (-1)</option>
    <option value="-2">Long (-2)</option>
    <option value="-3">Extreme (-3)</option>
	<option value="-4">Same Hex (-1/-2)</option>
  </select>
  <br />
  <div style="display: inline-block; width: 150px">Target Size:</div>
  <select id="targetSize" />
    <option value="0">Normal (+0)</option>
    <option value="2">Large (+2)</option>
    <option value="-2">Small (-2)</option>
  </select>
  <br />
   <div style="display: inline-block; width: 150px">Target Position:</div>
  <select id="targetPosition" />
    <option value="0">No concealment (+0)</option>
    <option value="-1">Light Concealment (-1)</option>
    <option value="-2">Heavy Concealment (-2)</option>
  </select>
   <br />
    <div style="display: inline-block; width: 150px">Time:</div>
   <select id="time" />
    <option value="0">Normal (+0)</option>
    <option value="-1">Dim (-1)</option>
    <option value="-2">Dark(-2)</option>
  </select>
   <br />
    <div style="display: inline-block; width: 150px">Weather:</div>
   <select id="weather" />
    <option value="0">Normal (+0)</option>
    <option value="-1">Rainy/Windy (-1)</option>
    <option value="-2">Rainy+Windy (-2)</option>
  </select>
  <br />
	<label>
	<input type="checkbox" id="targetProne"/>
	Target Prone?
	</label>
    <br />
	<label>
	<input type="checkbox" id="targetMoving"/>
	Target Moving?
	</label>
    <br />
	<label>
	<input type="checkbox" id="targetInFullCover"/>
	Target in Full cover?
	</label>
    <br />
	<label>
	<input type="checkbox" id="defenseLessSameHex"/>
	Target Defenceless in same hex?
	</label>
    <br />
	<label>
	<input type="checkbox" id="shooterMoving"/>
	Shooting from moving vehicle?
	</label>
    <br />
	<label>
	<input type="checkbox" id="shooterElevated"/>
	Shooter Elevated?
	</label>
    <br />
	<label>
	<input type="checkbox" id="calledShot" text="calledShot? IDK"/>
	Called Shot?
	</label>
`;

const dialog = new Dialog({
	title: "Aiming Calculator",
	content: form,
	buttons: {
    use: {
      label: "Apply",
      callback: applyAccuracy
    }
  },
	render: html => html[0].querySelectorAll("select, input").forEach(n => {n.addEventListener("change", function changeFunction(event){updateAccuracy(html)});}),

}).render(true);




 let whisper = false;
  
function applyAccuracy(html) {
  let checkOptions = [];
  const weaponType = (html.find(`select#weaponType`)[0].value);
  let aimType = parseInt(html.find(`select#aimType`)[0].value); // Depending on weapon system 
  if ((aimType == 1) && weaponType == "Rifle (Optics +2)") {aimType = 2};
  let distance = parseInt(html.find(`select#distance`)[0].value); // Depending on weapon system
  if ((distance == -4) && weaponType == "Carbine/Pistol/SMG") {distance = -1};
  if ((distance == -4) && weaponType == "Rifle") {distance = -2};
  if ((distance == -4) && weaponType == "Rifle (Optics +1)") {distance = -2};
  if ((distance == -4) && weaponType == "Rifle (Optics +2)") {distance = -2};
  const targetSize = parseInt(html.find(`select#targetSize`)[0].value);
  const targetPosition = parseInt(html.find(`select#targetPosition`)[0].value);
  const time = parseInt(html.find(`select#time`)[0].value);
  const weather = parseInt(html.find(`select#weather`)[0].value);

  const targetIsProne = html.find(`input#targetProne`)[0].checked;  // -1
  const targetIsMoving = html.find(`input#targetMoving`)[0].checked;  // -1
  const targetIsInFullCover = html.find(`input#targetInFullCover`)[0].checked; // -3
  const targetIsDefenseLessSameHex = html.find(`input#defenseLessSameHex`)[0].checked; // +3
  const shooterIsMoving = html.find(`input#shooterMoving`)[0].checked; // -2
  const shooterIsElevated = html.find(`input#shooterElevated`)[0].checked; // +1 
  const isCalledShot = html.find(`input#calledShot`)[0].checked; // -2 
	
  let aimModifierTotal = aimType + distance + targetSize + targetPosition + time + weather;
  if (targetIsProne) { aimModifierTotal = aimModifierTotal - 1; checkOptions+=`<br> Target is prone. (-1)`}
  if (targetIsMoving) { aimModifierTotal = aimModifierTotal - 1; checkOptions+=`<br> Target is moving. (-1)`}
  if (targetIsInFullCover) { aimModifierTotal = aimModifierTotal - 3; checkOptions+=`<br> Target is in full cover. (-3)`}
  if (targetIsDefenseLessSameHex) { aimModifierTotal = aimModifierTotal + 3; checkOptions+=`<br> Target is defenseless in the same hex. (+3)`}
  if (shooterIsMoving) { aimModifierTotal = aimModifierTotal - 2; checkOptions+=`<br> Shooting from moving vehicle. (-2)`}
  if (shooterIsElevated) { aimModifierTotal = aimModifierTotal + 1; checkOptions+=`<br> Shooter is elevated. (+1)`}
  if (isCalledShot) { aimModifierTotal = aimModifierTotal - 2; checkOptions+=`<br> Called shot. (-2)`}
  
  let outPutText = `
  <div><h2 style="background-color:#964B00;padding:5px 2px;color:#fff;border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;margin-bottom:0;">Accuracy Modifiers</h2><div style="border:1px solid #000;background-color:#fff;padding:4px;">
	<h3 style= "">TOTAL : ${aimModifierTotal} </br></h2>
	Type : ${weaponType} 
	<table style="width:100%">
		<td>Check</td>
		<td>Modifier</td>
		<tr>
			<td>Aiming</td>
			<td>${aimType}</td>
		</tr>
		<tr>
			<td>Distance</td>
			<td>${distance}</td>
		</tr>
		<tr>
			<td>Target Size</td>
			<td>${targetSize}</td>
		</tr>
		<tr>
			<td>Target Position</td>
			<td>${targetPosition}</td>
		</tr>
		<tr>
			<td>Lighting</td>
			<td>${time}</td>
		</tr>
		<tr>
			<td>Weather</td>
			<td>${weather}</td>
		</tr>
	</table>
	<h5 style="">${checkOptions}</h5>
 </div></div>
  `;
	let chatData = {
		content: outPutText,
	};
	ChatMessage.create(chatData, {});
	console.log (outPutText);
};

function updateAccuracy(html) {
  let checkOptions = [];
  const weaponType = (html.find(`select#weaponType`)[0].value);
  let aimType = parseInt(html.find(`select#aimType`)[0].value); // Depending on weapon system 
  if ((aimType == 1) && weaponType == "Rifle (Optics +2)") {aimType = 2};
  let distance = parseInt(html.find(`select#distance`)[0].value); // Depending on weapon system
  if ((distance == -4) && weaponType == "Carbine/Pistol/SMG") {distance = -1};
  if ((distance == -4) && weaponType == "Rifle") {distance = -2};
  if ((distance == -4) && weaponType == "Rifle (Optics +1)") {distance = -2};
  if ((distance == -4) && weaponType == "Rifle (Optics +2)") {distance = -2};
  const targetSize = parseInt(html.find(`select#targetSize`)[0].value);
  const targetPosition = parseInt(html.find(`select#targetPosition`)[0].value);
  const time = parseInt(html.find(`select#time`)[0].value);
  const weather = parseInt(html.find(`select#weather`)[0].value);

  const targetIsProne = html.find(`input#targetProne`)[0].checked;  // -1
  const targetIsMoving = html.find(`input#targetMoving`)[0].checked;  // -1
  const targetIsInFullCover = html.find(`input#targetInFullCover`)[0].checked; // -3
  const targetIsDefenseLessSameHex = html.find(`input#defenseLessSameHex`)[0].checked; // +3
  const shooterIsMoving = html.find(`input#shooterMoving`)[0].checked; // -2
  const shooterIsElevated = html.find(`input#shooterElevated`)[0].checked; // +1 
  const isCalledShot = html.find(`input#calledShot`)[0].checked; // -2 
	
  let aimModifierTotal = aimType + distance + targetSize + targetPosition + time + weather;
  if (targetIsProne) { aimModifierTotal = aimModifierTotal - 1; checkOptions+=`<br> Target is prone. (-1)`}
  if (targetIsMoving) { aimModifierTotal = aimModifierTotal - 1; checkOptions+=`<br> Target is moving. (-1)`}
  if (targetIsInFullCover) { aimModifierTotal = aimModifierTotal - 3; checkOptions+=`<br> Target is in full cover. (-3)`}
  if (targetIsDefenseLessSameHex) { aimModifierTotal = aimModifierTotal + 3; checkOptions+=`<br> Target is defenseless in the same hex. (+3)`}
  if (shooterIsMoving) { aimModifierTotal = aimModifierTotal - 2; checkOptions+=`<br> Shooting from moving vehicle. (-2)`}
  if (shooterIsElevated) { aimModifierTotal = aimModifierTotal + 1; checkOptions+=`<br> Shooter is elevated. (+1)`}
  if (isCalledShot) { aimModifierTotal = aimModifierTotal - 2; checkOptions+=`<br> Called shot. (-2)`}
  
    let outPutText = `
	Current Modifier : ${aimModifierTotal} 
	`;
  ui.notifications.info (outPutText)
};