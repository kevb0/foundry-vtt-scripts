const form = `
	<table>
		<td style="text-align: left;width: 33%;">Slow Action</td>
		<td style="text-align: left;width: 33%;">Fast Action</td>
		<td style="text-align: left;width: 33%;">Free Action</td>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="getItemFromBackpack"/>Get item from backpack</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Run"/>Run</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Droptotheground"/>Drop to the ground	</label></td>
		</tr>
			<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="persuade"/>Persuade</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Dropbackpack"/>Drop backpack</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Drophelditem"/>Drop held item</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="melee"/>Unarmed / Melee attack</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Seekcoverfullorpartial"/>Seek cover (full or partial)</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Gofrompartialtofullcover"/>Go from partial to full cover</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="shoot"/>Shoot firearm</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Gofromfulltopartialcover"/>Go from full to partial cover</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Shoutafewwords"/>Shout a few words</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="clearJam"/>Clear Jam</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Getup"/>Get up</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="OtherFree"/>Other</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="aimOptics"/>Aim through optics</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Aim"/>Aim</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="ShootBow"/>Shoot Bow</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Shove"/>Shove</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="AimHowitzerMortar"/>Aim Howitzer/Mortar</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Disarm"/>Disarm</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Fireheavyweapon"/>Fire heavy weapon</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Grappleattack"/>Grapple attack</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Directindirectfire"/>Direct indirect fire	</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Retreat"/>Retreat</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Firstaid"/>First aid</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Assumeoverwatchposition"/>Assume overwatch position</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Rally"/>Get item from backpack</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Reloadslowactioniffailed"/>Reload (slow action if failed)</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Enterexitvehicle"/>Enter/exit vehicle</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="Useitem"/>Use item</label></td>
		</tr>
		<tr>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="OtherSlow"/>Other</label></td>
			<td style="text-align: left;width: 33%;"><input type="checkbox" id="OtherFast"/>Other</label></td>
		</tr>
	<table>
`;

const dialog = new Dialog({
	title: "Actions",
	content: form,
	buttons: {
    use: {
      label: "Apply",
      callback: applyActions
    }
  }

}).render(true);


function applyActions(html) {
  let slowActions = [];
  let fastActions = [];
  let freeActions = [];
  
 if ( html.find(`input#getItemFromBackpack`)[0].checked ) {slowActions+=`<br> Get item from backpack`}
 if ( html.find(`input#persuade`)[0].checked ) {slowActions+=`<br> Persuade`}
 if ( html.find(`input#shoot`)[0].checked ) {slowActions+=`<br> Fire weapon`}
 if ( html.find(`input#clearJam`)[0].checked ) {slowActions+=`<br> Clear jam`}
 if ( html.find(`input#aimOptics`)[0].checked ) {slowActions+=`<br> Aim through optics`}
 if ( html.find(`input#ShootBow`)[0].checked ) {slowActions+=`<br> Shoot bow`}
 if ( html.find(`input#AimHowitzerMortar`)[0].checked ) {slowActions+=`<br> Aim Howitzer/Mortar`}
 if ( html.find(`input#Fireheavyweapon`)[0].checked ) {slowActions+=`<br> Fire heavy weapon`}
 if ( html.find(`input#Directindirectfire`)[0].checked ) {slowActions+=`<br> Direct indirect fire`}
 if ( html.find(`input#Firstaid`)[0].checked ) {slowActions+=`<br> First aid`}
 if ( html.find(`input#Rally`)[0].checked ) {slowActions+=`<br> Rally`}
 if ( html.find(`input#Enterexitvehicle`)[0].checked ) {slowActions+=`<br> Enter/exit vehicle`}
 if ( html.find(`input#OtherSlow`)[0].checked ) {slowActions+=`<br> Other`}


 if ( html.find(`input#Run`)[0].checked ) {fastActions+=`<br> Run`}
 if ( html.find(`input#Dropbackpack`)[0].checked ) {fastActions+=`<br> Drop backpack`}
 if ( html.find(`input#Seekcoverfullorpartial`)[0].checked ) {fastActions+=`<br> Seek cover (full or partial)`}
 if ( html.find(`input#Gofromfulltopartialcover`)[0].checked ) {fastActions+=`<br> Go from full to partial cover`}
 if ( html.find(`input#Getup`)[0].checked ) {fastActions+=`<br> Get up`}
 if ( html.find(`input#Aim`)[0].checked ) {fastActions+=`<br> Aim`}
 if ( html.find(`input#Shove`)[0].checked ) {fastActions+=`<br> Shove`}
 if ( html.find(`input#Disarm`)[0].checked ) {fastActions+=`<br> Disarm`}
 if ( html.find(`input#Grappleattack`)[0].checked ) {fastActions+=`<br> Grapple attack`}
 if ( html.find(`input#Retreat`)[0].checked ) {fastActions+=`<br> Retreat`}
 if ( html.find(`input#Assumeoverwatchposition`)[0].checked ) {fastActions+=`<br> Assume overwatch position`}
 if ( html.find(`input#Reloadslowactioniffailed`)[0].checked ) {fastActions+=`<br> Reload`}
 if ( html.find(`input#Useitem`)[0].checked ) {fastActions+=`<br> Use item`}
 if ( html.find(`input#OtherFast`)[0].checked ) {fastActions+=`<br> Other`}
 
 if ( html.find(`input#Droptotheground`)[0].checked ) {freeActions+=`<br> Drop to the ground`}
 if ( html.find(`input#Drophelditem`)[0].checked ) {freeActions+=`<br> Drop held item`}
 if ( html.find(`input#Gofrompartialtofullcover`)[0].checked ) {freeActions+=`<br> Go from partial to full cover`}
 if ( html.find(`input#Shoutafewwords`)[0].checked ) {freeActions+=`<br> Shout a few words`}
 if ( html.find(`input#OtherFree`)[0].checked ) {freeActions+=`<br> Other`}


  let outPutText = `
  <div><h4 style="background-color:#964B00;padding:2px 2px;color:#fff;border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;margin-bottom:0;">Slow Actions</h4><div style="border:1px solid #000;background-color:#fff;padding:2px;">
	<h5 style="">${slowActions}</h5>
 </div></div>
   <div><h4 style="background-color:#964B00;padding:2px 2px;color:#fff;border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;margin-bottom:0;">Fast Actions</h4><div style="border:1px solid #000;background-color:#fff;padding:2px;">
	<h5 style="">${fastActions}</h5>
 </div></div>
   <div><h4 style="background-color:#964B00;padding:2px 2px;color:#fff;border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;margin-bottom:0;">Free Actions</h4><div style="border:1px solid #000;background-color:#fff;padding:2px;">
	<h5 style="">${freeActions}</h5>
 </div></div>
  `;
	let chatData = {
		content: outPutText,
	};
	ChatMessage.create(chatData, {});
	console.log (outPutText);
};

