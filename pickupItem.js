
if (canvas.tokens.controlled.length === 0)
  return ui.notifications.error("You need to have a token selected to add the item to!");

let applyChanges = false;
let pickUpableItems = [];
let pickerUpper = actor.getActiveTokens(true)[0];

canvas.notes.objects.children.forEach((i) => {
	const ray = new Ray(pickerUpper.center,i.center);
	if (ray.distance < 130) { //  && i.data.data.header.active 
		pickUpableItems.push(i);
	}
});

if (pickUpableItems.length === 0)
  return ui.notifications.error("Could not find any valid objects close enough to your token!");

let selectedNote = pickUpableItems[0];
let selectedJournalEntry = game.journal.get(pickUpableItems[0].data.entryId);

const item = game.items.getName(selectedJournalEntry.data.name);
await actor.createEmbeddedDocuments('Item', [item.toObject()])

await selectedJournalEntry.delete();

await canvas.scene.deleteEmbeddedDocuments("Note",[selectedNote.id]);
