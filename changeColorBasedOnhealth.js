let updates = canvas.tokens.placeables
  .filter(token=> token.actor.data.type === `vehicle`)
  .map(token=> {
    let { value, min, max } = token.actor.data.data.stats.hullTrauma;
    let percent = Math.floor((parseInt(value)/max)*100);

    let tint = percent >= 70 ? `#008000`
      : percent >= 30 && percent < 70 ? `#ffff00`
      : `#FF0000`;
  
    return { _id : token.id, tint};
  });
  
canvas.tokens.updateMany(updates);