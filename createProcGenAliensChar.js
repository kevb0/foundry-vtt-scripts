/*
	This pulls data from chartopia and creates an actor with random characters. The chart can be found here - https://chartopia.d12dev.com/chart/33313/
	It also can pull procedually generated faces from https://generated.photos/


*/
var chartId = 33313; // Chart number of alien chart
var apiKey = "KEYGOESHERE;"; // API Key from https://generated.photos/


const form = `
  <label>
  	<input type="checkbox" id="generatePhoto"/>
    Generate and apply procedural image?
	</label>
`;

const dialog = new Dialog({
  title: "Generate Alien RPG Character",
  content: form,
  buttons: {
    use: {
      label: "Generate!",
      callback: roll
    }
  }
}).render(true);

var rootUrl = "https://chartopia.d12dev.com/api/";

function roll(html) {
    let request = new XMLHttpRequest();
    request.open('POST', rootUrl + `charts/${chartId}/roll/`, true);

  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
    var result = request.responseText // Hacky solution to interpet the chartiopia data as json
    var jsonObject = JSON.parse(result);
	console.log("result:" + (jsonObject.results));
	var parsedJSON = jsonObject.results[0].replace("$$$", "{").replace("%%%", "}");
	var objectData = JSON.parse(parsedJSON)
	console.log( (((objectData))));
  (async ()=>{  
   let actor = await Actor.create({ // Create the actor
      name: objectData.NAME,
      type: "character",
      sort: 12000,
      data: { },
      token: {},
      items: [],
      flags: {}
    }); 
	
	const shouldGeneratePicture = html.find(`input#generatePhoto`)[0].checked;
	
	if (shouldGeneratePicture) {
	console.log(shouldGeneratePicture);
	var photoURL = "https://api.generated.photos/api/v1/faces?api_key=";
	  let request = new XMLHttpRequest();
	  request.open('GET', photoURL+apiKey+"&per_page=1"+"&gender="+objectData.gender+"&age="+objectData.ageType, true);
	  request.onload = function() {
		console.log(request.status);
		if (request.status >= 200 && request.status < 400) {
		var response = JSON.parse(request.responseText);
		const image = response.faces[0].urls[4];
		for (let key in image) {
			var imageLink = image[key];
			actor.update({
			'img':  image[key]
			});
		}
		console.log(imageLink);
		} else {
		  console.log("Picture Server error.");
		}
	  };
	  request.send();
	};

	actor.update({
		'data.attributes.str.value': objectData.Strength,
		'data.header.health.value': objectData.Strength,
		'data.attributes.agl.value': objectData.Agility,
		'data.attributes.emp.value': objectData.Empathy,
		'data.attributes.wit.value': objectData.Wits,
		'data.skills.heavyMach.value': objectData.HeavyMachinery,
		'data.skills.closeCbt.value': objectData.CloseCombat,
		'data.skills.stamina.value': objectData.Stamina,
		'data.skills.rangedCbt.value': objectData.RangedCombat,
		'data.skills.mobility.value': objectData.Mobility,
		'data.skills.piloting.value': objectData.Piloting,
		'data.skills.command.value': objectData.Command,
		'data.skills.manipulation.value': objectData.Manipulation,
		'data.skills.medicalAid.value': objectData.MedicalAid,
		'data.skills.observation.value': objectData.Observation,
		'data.skills.survival.value': objectData.Survival,
		'data.skills.comtech.value': objectData.Comtech,
		'data.general.appearance.value': ("Age:" + objectData.age + "\n" + objectData.Personality),
		'data.general.sigItem.value': objectData.SignatureItem,
		'data.general.agenda.value': decodeURIComponent(encodeURIComponent(objectData.PersonalAgenda)),
		'data.general.career.value': parseInt (objectData.CAREERNUM),
		'data.adhocitems': objectData.Inventory
	});
	let talent = game.items.find(a => a.name == decodeURIComponent(encodeURIComponent(objectData.Talents))); // Find the Foundry Item (Talent) 
	console.log(talent);
	await actor.createEmbeddedDocuments('Item', [talent.toObject()]);
	})();
	
    } else {
      console.log("Chartopia Server error.");
    }
  };
  request.onerror = function() {
    console.log("Error getting result.");
  };
  request.send();
} 

