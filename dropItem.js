/** 
 * Drops a random item from the player's inventory. It then creates a journal entry at the player's position so that they can pick it up.
 
 */

if (canvas.tokens.controlled.length === 0)
  return ui.notifications.error("Please select a token first");

let applyChanges = false;
let droppableItems = [];
let checkOptions = [];



actor.items.forEach((i) => {
	console.log(i);
	if ((i.type === 'item' || i.type === 'weapon' || i.type === 'armor' || i.type === 'gear' || i.type  === 'grenade')) { //  && i.data.data.header.active 
		droppableItems.push(i);
	}
});

// Build checkbox list 
droppableItems.forEach((i) => {
  let checked = false;
  checkOptions+=`
    <br>
    <input type="checkbox" name="${i.id}" id="${i.id}" value="${i.name}" ${checked}>\n
    <label for="${i.id}">${i.name}</label>
  `
});

new Dialog({
  title:"Drop Items",
  content:`<br> Items to drop : ${checkOptions} <br>`,
  buttons:{
    Drop:{   
      label:"Drop",
      callback: async (html) => {
		var itemsToDrop = [];
		for ( let i of droppableItems ) {
			if (html.find('[name="'+i.id+'"]')[0].checked){
			applyChanges=true;
			itemsToDrop.push(i); 
			}
		}
		if(!applyChanges)return;
			for ( let q of itemsToDrop ) {

				const journalName = `${q.name}`;
				const droppedObjectsEntry = await JournalEntry.create({
				  name: journalName, 
				  img: q.img,
				  content: `A ${q.name}. It was left here on the ground. <br>Looks like it's from from ${actor.name} <br><br> @Macro[PickUpObjectFromJournal]{Pick it up!} `,
				  'permission.default': CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER
				  });


				
				let droppedItem = actor.items.find(item => item.name == q.name);
				  await droppedItem.update({"system.qty": droppedItem.system.qty - 1});
				  console.log (droppedItem);
				  if(droppedItem.system.qty < 1){
					droppedItem.delete();
				  };
 
			   let outPutText = `
				${actor.name} dropped ${q.name}.
			   `;
			   
				let chatData = {
					content: outPutText,
				};
				ChatMessage.create(chatData, {});
	
	
				canvas.scene.createEmbeddedDocuments("Note",[{
					entryId: droppedObjectsEntry.id,
					x:(canvas.tokens.controlled[0].transform.position.x),
					y:(canvas.tokens.controlled[0].transform.position.y),
					icon: q.img,
					iconSize: 16,
					iconTint: "#ffffff",
					text: `Dropped ${q.name}`,
					fontSize: 16,
					textAnchor: CONST.TEXT_ANCHOR_POINTS.BOTTOM,
					textColor: "#ffffff"
				}]);
			  console.log(droppedObjectsEntry);
			}
		}
    }
  }
}).render(true);

